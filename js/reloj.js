(function(){
  
  let horaActual = function(){
    fecha = new Date(),
    hora = fecha.getHours();
    ampm,
    minuto = fecha.getMinutes();
    segundo = fecha.getSeconds();
    diaSemana = fecha.getDay();
    dia = fecha.getDate();
    mes = fecha.getMonth();
    anio = fecha.getFullYear();
    
    
  let pHora = document.getElementById("hora"),
    pAmPm = document.getElementById("ampm"),
    pMinuto = document.getElementById("minuto"),
    pSegundo = document.getElementById("segundo"),
    pDiaSemana = document.getElementById("diaSemana"),
    pDia = document.getElementById("dia"),
    pMes = document.getElementById("mes"),
    pAnio = document.getElementById("anio");
    
  const Semana = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"];
  const meses = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
  
  pDiaSemana.textContent = Semana[diaSemana];
  
  pDia.textContent = dia;
  
  pMes.textContent = meses[mes];
  
  pAnio.textContent = anio;
  
  if (hora >= 12) {
    hora = hora-12;
    ampm = "PM";
  } else {
    ampm = "AM";
  }
  
  if (hora == 0) {
    hora = 12;
  };
  
  pHora.textContent = hora;
  pAmPm.textContent = ampm;


  if (minuto < 10) {
    minuto = "0"+minuto;
  }
  if (segundo < 10) {
    segundo = "0"+segundo;
  }
  pMinuto.textContent = minuto;
  pSegundo.textContent = segundo;
    
  };
  
  horaActual();
  let intervalo = setInterval(() => {
    horaActual();
    
  }, 1000);
}())
